Index: refpolicy-2.20250107/policy/modules/roles/sysadm.te
===================================================================
--- refpolicy-2.20250107.orig/policy/modules/roles/sysadm.te
+++ refpolicy-2.20250107/policy/modules/roles/sysadm.te
@@ -713,6 +713,10 @@ optional_policy(`
 ')
 
 optional_policy(`
+	needrestart_run(sysadm_t, sysadm_r)
+')
+
+optional_policy(`
 	nessus_admin(sysadm_t, sysadm_r)
 ')
 
Index: refpolicy-2.20250107/policy/modules/system/needrestart.fc
===================================================================
--- /dev/null
+++ refpolicy-2.20250107/policy/modules/system/needrestart.fc
@@ -0,0 +1 @@
+/usr/sbin/needrestart	--	gen_context(system_u:object_r:needrestart_exec_t,s0)
Index: refpolicy-2.20250107/policy/modules/system/needrestart.if
===================================================================
--- /dev/null
+++ refpolicy-2.20250107/policy/modules/system/needrestart.if
@@ -0,0 +1,52 @@
+## <summary>Policy for needrestart utility.</summary>
+##
+## <desc>
+## Utility to restart daemons after system updates
+## </desc>
+
+########################################
+## <summary>
+##      Execute needrestart in the user role
+## </summary>
+## <param name="domain">
+##      <summary>
+##      Domain allowed to transition.
+##      </summary>
+## </param>
+## <param name="role">
+##      <summary>
+##      Role allowed access.
+##      </summary>
+## </param>
+## <rolecap/>
+#
+interface(`needrestart_run',`
+	gen_require(`
+		attribute_role needrestart_roles;
+		type needrestart_exec_t, needrestart_t;
+	')
+
+	domtrans_pattern($1, needrestart_exec_t, needrestart_t)
+	roleattribute $2 needrestart_roles;
+	systemd_run_passwd_agent(needrestart_t, $2)
+')
+
+########################################
+## <summary>
+##	Execute needrestart programs in the needrestart domain.
+## </summary>
+## <param name="domain">
+##	<summary>
+##	Domain allowed to transition.
+##	</summary>
+## </param>
+#
+interface(`needrestart_domtrans',`
+	gen_require(`
+		type needrestart_t, needrestart_exec_t;
+	')
+
+	corecmd_search_bin($1)
+	domtrans_pattern($1, needrestart_exec_t, needrestart_t)
+')
+
Index: refpolicy-2.20250107/policy/modules/system/needrestart.te
===================================================================
--- /dev/null
+++ refpolicy-2.20250107/policy/modules/system/needrestart.te
@@ -0,0 +1,100 @@
+policy_module(needrestart)
+
+########################################
+#
+# Declarations
+#
+
+type needrestart_t;
+type needrestart_exec_t;
+init_system_domain(needrestart_t, needrestart_exec_t)
+attribute_role needrestart_roles;
+role needrestart_roles types needrestart_t;
+
+type needrestart_tmp_t;
+files_tmp_file(needrestart_tmp_t)
+
+########################################
+#
+# Local policy
+#
+
+dontaudit needrestart_t self:capability { net_admin };
+# sys_ptrace is for getting stats of processes
+# sys_resource is for sd-askpwagent
+allow needrestart_t self:capability { dac_read_search sys_ptrace sys_resource };
+allow needrestart_t self:capability2 checkpoint_restore;
+allow needrestart_t self:process { getcap signal setrlimit };
+allow needrestart_t self:fifo_file rw_file_perms;
+
+# for /proc/*/cwd etc
+allow needrestart_t self:cap_userns sys_ptrace;
+
+allow needrestart_t needrestart_tmp_t:dir manage_dir_perms;
+files_tmp_filetrans(needrestart_t, needrestart_tmp_t, { dir })
+
+kernel_read_kernel_sysctls(needrestart_t)
+kernel_read_fs_sysctls(needrestart_t)
+kernel_read_state(needrestart_t)
+kernel_read_system_state(needrestart_t)
+
+kernel_getattr_proc(needrestart_t)
+
+dev_read_urand(needrestart_t)
+dev_read_rand(needrestart_t)
+dev_read_sysfs(needrestart_t)
+
+corecmd_exec_all_executables(needrestart_t)
+domain_read_all_domains_state(needrestart_t)
+
+corecmd_exec_bin(needrestart_t)
+
+# for /var/cache/debconf/config.dat-new
+files_dontaudit_rw_var_files(needrestart_t)
+
+# for /etc/needrestart files
+files_exec_etc_files(needrestart_t)
+
+files_list_boot(needrestart_t)
+files_read_kernel_img(needrestart_t)
+
+# for /var/cache/debconf/config.dat do we need a different label for that?
+files_read_var_files(needrestart_t)
+
+files_read_etc_files(needrestart_t)
+
+files_read_usr_symlinks(needrestart_t)
+files_read_usr_files(needrestart_t)
+
+files_search_var_lib(needrestart_t)
+
+fs_getattr_cgroup(needrestart_t)
+fs_getattr_nsfs_files(needrestart_t)
+
+init_admin(needrestart_t)
+init_manage_all_units(needrestart_t)
+init_read_state(needrestart_t)
+init_start_generic_units(needrestart_t)
+init_unix_stream_socket_connectto(needrestart_t)
+
+libs_exec_lib_files(needrestart_t)
+
+miscfiles_read_localization(needrestart_t)
+
+term_getattr_ptmx(needrestart_t)
+term_getattr_unallocated_ttys(needrestart_t)
+term_read_console(needrestart_t)
+
+userdom_use_user_ptys(needrestart_t)
+
+optional_policy(`
+	dpkg_read_db(needrestart_t)
+')
+
+optional_policy(`
+	ssh_use_sshd_pidfds(needrestart_t)
+')
+
+optional_policy(`
+	systemd_exec_systemctl(needrestart_t)
+')
Index: refpolicy-2.20250107/policy/modules/system/unconfined.te
===================================================================
--- refpolicy-2.20250107.orig/policy/modules/system/unconfined.te
+++ refpolicy-2.20250107/policy/modules/system/unconfined.te
@@ -163,6 +163,10 @@ optional_policy(`
 ')
 
 optional_policy(`
+	needrestart_run(unconfined_t, unconfined_r)
+')
+
+optional_policy(`
 	oddjob_domtrans_mkhomedir(unconfined_t)
 ')
 
Index: refpolicy-2.20250107/policy/modules/admin/apt.te
===================================================================
--- refpolicy-2.20250107.orig/policy/modules/admin/apt.te
+++ refpolicy-2.20250107/policy/modules/admin/apt.te
@@ -159,6 +159,10 @@ optional_policy(`
 ')
 
 optional_policy(`
+	needrestart_domtrans(apt_t)
+')
+
+optional_policy(`
 	nis_use_ypbind(apt_t)
 ')
 
